<form action="" method="post">
  @csrf
  @error('email') {{ $message }} @enderror
  <input type="text" name="email" id="email" placeholder="email">
  @error('password') {{ $message }} @enderror
  <input type="password" name="password" id="password" placeholder="password">
  <button type="submit">Login</button>
</form>
